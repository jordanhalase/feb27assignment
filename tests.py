import unittest
import card, deck

suite = unittest.TestSuite()


# Card tests

# Mya
# Ian
class CardInitCorrectRankAndSuit(unittest.TestCase):
    pass


suite.addTest(unittest.makeSuite(CardInitCorrectRankAndSuit))

# Parker
class CardInitRaiseValueError(unittest.TestCase):
    pass


suite.addTest(unittest.makeSuite(CardInitRaiseValueError))


# Sam
class CardGetSuite(unittest.TestCase):
    pass


suite.addTest(unittest.makeSuite(CardGetSuite))


# Parker
class CardGetRank(unittest.TestCase):
    pass


suite.addTest(unittest.makeSuite(CardGetRank))


# Jordan
class CardGetValue(unittest.TestCase):
    pass


suite.addTest(unittest.makeSuite(CardGetValue))


# Jordan
class CardStr(unittest.TestCase):
    pass


suite.addTest(unittest.makeSuite(CardStr))


# deck tests
# you must add these tests to the suite

# Mya
# Ian
class DeckInit(unittest.TestCase):
    pass


# Sam
class DeckCount(unittest.TestCase):
    pass


# Spencer
class DeckDrawValue(unittest.TestCase):
    pass


# Spencer
class DeckDrawException(unittest.TestCase):
    pass


# Sam
class DeckShuffle(unittest.TestCase):
    pass
